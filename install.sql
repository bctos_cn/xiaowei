/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : xiaowei_git

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 21/12/2020 17:33:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wp_action
-- ----------------------------
DROP TABLE IF EXISTS `wp_action`;
CREATE TABLE `wp_action`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '行为说明',
  `remark` char(140) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '行为规则',
  `log` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '日志规则',
  `type` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '状态',
  `update_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统行为表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_action
-- ----------------------------

-- ----------------------------
-- Table structure for wp_action_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_action_log`;
CREATE TABLE `wp_action_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '行为id',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '执行用户id',
  `action_ip` bigint(20) NOT NULL COMMENT '执行行为者ip',
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `record_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '触发行为的数据id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '执行行为的时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_ip_ix`(`action_ip`) USING BTREE,
  INDEX `action_id_ix`(`action_id`) USING BTREE,
  INDEX `user_id_ix`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行为日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for wp_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_admin_log`;
CREATE TABLE `wp_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL COMMENT '用户ID',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户IP地址',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志内容',
  `mod` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用名',
  `cTime` int(10) NULL DEFAULT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for wp_api_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_api_log`;
CREATE TABLE `wp_api_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_at` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'param',
  `server_ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'server_ip',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_api_log
-- ----------------------------

-- ----------------------------
-- Table structure for wp_app_shop
-- ----------------------------
DROP TABLE IF EXISTS `wp_app_shop`;
CREATE TABLE `wp_app_shop`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_at` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `update_at` int(10) NULL DEFAULT 0 COMMENT '更新时间',
  `uid` int(10) NULL DEFAULT 0 COMMENT '用户',
  `status` tinyint(2) NULL DEFAULT 0 COMMENT '状态',
  `type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '扩展类型',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '扩展名',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `img` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '截图',
  `attach` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装包',
  `is_top` int(10) NULL DEFAULT 0 COMMENT '置顶',
  `view_count` int(10) NULL DEFAULT 0 COMMENT '浏览数',
  `price` int(10) NULL DEFAULT 0 COMMENT '价格',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '扩展封面',
  `download_count` int(10) NULL DEFAULT 0 COMMENT '下载数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_app_shop
-- ----------------------------

-- ----------------------------
-- Table structure for wp_app_shop_user
-- ----------------------------
DROP TABLE IF EXISTS `wp_app_shop_user`;
CREATE TABLE `wp_app_shop_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `create_at` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `uid` int(10) NULL DEFAULT 0 COMMENT '用户',
  `status` tinyint(2) NULL DEFAULT 0 COMMENT '支付状态',
  `app_id` int(10) NULL DEFAULT 0 COMMENT '扩展ID',
  `price` int(10) NULL DEFAULT 0 COMMENT '支付积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of wp_app_shop_user
-- ----------------------------

-- ----------------------------
-- Table structure for wp_app_shop_web
-- ----------------------------
DROP TABLE IF EXISTS `wp_app_shop_web`;
CREATE TABLE `wp_app_shop_web`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '站名',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT 'Logo',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网址',
  `wealth` int(10) NULL DEFAULT 0 COMMENT '余额',
  `web_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权许可证',
  `uid` int(10) NULL DEFAULT 0 COMMENT '用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_app_shop_web
-- ----------------------------

-- ----------------------------
-- Table structure for wp_apps
-- ----------------------------
DROP TABLE IF EXISTS `wp_apps`;
CREATE TABLE `wp_apps`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置',
  `author` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '版本号',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装时间',
  `has_adminlist` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否有后台列表',
  `type` tinyint(1) NULL DEFAULT 0 COMMENT '插件类型 0 普通插件 1 微信插件 2 易信插件',
  `cate_id` int(11) NULL DEFAULT NULL,
  `is_show` tinyint(2) NULL DEFAULT 1,
  `theme` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题名称，为空时取默认主题',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `sti`(`status`, `is_show`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 301 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '微信插件表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_apps
-- ----------------------------

-- ----------------------------
-- Table structure for wp_area
-- ----------------------------
DROP TABLE IF EXISTS `wp_area`;
CREATE TABLE `wp_area`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地区名',
  `pid` int(10) NULL DEFAULT 0 COMMENT '上级编号',
  `sort` int(10) NULL DEFAULT 0 COMMENT '排序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 659 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_area
-- ----------------------------

-- ----------------------------
-- Table structure for wp_auth_extend
-- ----------------------------
DROP TABLE IF EXISTS `wp_auth_extend`;
CREATE TABLE `wp_auth_extend`  (
  `group_id` mediumint(10) UNSIGNED NOT NULL COMMENT '用户id',
  `extend_id` mediumint(8) UNSIGNED NOT NULL COMMENT '扩展表中数据的id',
  `type` tinyint(1) UNSIGNED NOT NULL COMMENT '扩展类型标识 1:栏目分类权限;2:模型权限',
  UNIQUE INDEX `group_extend_type`(`group_id`, `extend_id`, `type`) USING BTREE,
  INDEX `uid`(`group_id`) USING BTREE,
  INDEX `group_id`(`extend_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组与分类的对应关系表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of wp_auth_extend
-- ----------------------------

-- ----------------------------
-- Table structure for wp_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `wp_auth_group`;
CREATE TABLE `wp_auth_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '图标',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述信息',
  `status` tinyint(2) NULL DEFAULT 1 COMMENT '状态',
  `type` tinyint(2) NULL DEFAULT 1 COMMENT '类型',
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '权限',
  `manager_id` int(10) NULL DEFAULT 0 COMMENT '管理员ID',
  `pbid` int(10) NULL DEFAULT 0 COMMENT 'pbid',
  `is_default` tinyint(1) NULL DEFAULT 0 COMMENT '是否默认自动加入',
  `qr_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信二维码',
  `wechat_group_id` int(10) NULL DEFAULT -1 COMMENT '微信端的分组ID',
  `wechat_group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信端的分组名',
  `wechat_group_count` int(10) NULL DEFAULT 0 COMMENT '微信端用户数',
  `is_del` tinyint(1) NULL DEFAULT 0 COMMENT '是否已删除',
  `menu_rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '菜单权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 196 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_auth_group
-- ----------------------------
INSERT INTO `wp_auth_group` VALUES (2, '超级管理员', NULL, '所有从公众号自动注册的粉丝用户都会自动加入这个用户组', 1, 0, '1,2,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,82,83,84,88,89,90,91,92,93,96,97,100,102,103,195', 0, NULL, 0, NULL, NULL, NULL, NULL, 0, '1,2,3,4,7,8,852,12');

-- ----------------------------
-- Table structure for wp_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `wp_auth_group_access`;
CREATE TABLE `wp_auth_group_access`  (
  `uid` int(10) NULL DEFAULT NULL COMMENT '用户id',
  `group_id` mediumint(8) UNSIGNED NOT NULL COMMENT '用户组id',
  `cTime` int(11) NULL DEFAULT NULL,
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of wp_auth_group_access
-- ----------------------------
INSERT INTO `wp_auth_group_access` VALUES (1, 2, NULL);

-- ----------------------------
-- Table structure for wp_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `wp_auth_rule`;
CREATE TABLE `wp_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则附加条件',
  `group` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '默认分组',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 280 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_auth_rule
-- ----------------------------
INSERT INTO `wp_auth_rule` VALUES (241, 'Admin/Rule/createRule', '权限节点管理', 1, '', '默认分组');
INSERT INTO `wp_auth_rule` VALUES (242, 'Admin/AuthManager/index', '用户组管理', 1, '', '默认分组');
INSERT INTO `wp_auth_rule` VALUES (243, 'Admin/User/index', '用户信息', 1, '', '用户管理');

-- ----------------------------
-- Table structure for wp_block
-- ----------------------------
DROP TABLE IF EXISTS `wp_block`;
CREATE TABLE `wp_block`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `page` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '页面',
  `block_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '块元素',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_block
-- ----------------------------

-- ----------------------------
-- Table structure for wp_blockchain_channel
-- ----------------------------
DROP TABLE IF EXISTS `wp_blockchain_channel`;
CREATE TABLE `wp_blockchain_channel`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `channel_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通道名称',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通道版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_blockchain_channel
-- ----------------------------

-- ----------------------------
-- Table structure for wp_cache_keys
-- ----------------------------
DROP TABLE IF EXISTS `wp_cache_keys`;
CREATE TABLE `wp_cache_keys`  (
  `table_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `key_rule` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `map_field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `data_field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `extra` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_cache_keys
-- ----------------------------
INSERT INTO `wp_cache_keys` VALUES ('wp_apps', 'wpcache_wp_apps_name-[name]', '{\"name\":\"admin\"}', 'id,status', '');
INSERT INTO `wp_cache_keys` VALUES ('wp_publics', 'wpcache_wp_publics_id-[id]', '{\"id\":\"1\"}', '', '');
INSERT INTO `wp_cache_keys` VALUES ('wp_menu', 'wpcache_wp_menu_is_hide-[is_hide]', '{\"is_hide\":\"0\"}', '', '');
INSERT INTO `wp_cache_keys` VALUES ('wp_apps', 'wpcache_wp_apps_status-[status]', '{\"status\":1}', '', '');
INSERT INTO `wp_cache_keys` VALUES ('wp_user', 'wpcache_wp_user_uid-[uid]', '{\"uid\":\"1\"}', '', '');
INSERT INTO `wp_cache_keys` VALUES ('wp_model', 'wpcache_wp_model_name-[name]', '{\"name\":\"site\"}', '', '');

-- ----------------------------
-- Table structure for wp_channel
-- ----------------------------
DROP TABLE IF EXISTS `wp_channel`;
CREATE TABLE `wp_channel`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '频道ID',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级频道ID',
  `title` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '频道标题',
  `url` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '频道连接',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '导航排序',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态',
  `target` tinyint(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '新窗口打开',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_channel
-- ----------------------------

-- ----------------------------
-- Table structure for wp_city
-- ----------------------------
DROP TABLE IF EXISTS `wp_city`;
CREATE TABLE `wp_city`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `manager_uids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cTime` int(11) NULL DEFAULT NULL,
  `logo` int(11) NULL DEFAULT NULL COMMENT '城市分站LOGO',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_city
-- ----------------------------

-- ----------------------------
-- Table structure for wp_comment
-- ----------------------------
DROP TABLE IF EXISTS `wp_comment`;
CREATE TABLE `wp_comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aim_table` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论关联数据表',
  `aim_id` int(10) NULL DEFAULT 0 COMMENT '评论关联ID',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '评论时间',
  `follow_id` int(10) NULL DEFAULT 0 COMMENT 'follow_id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评论内容',
  `is_audit` tinyint(2) NULL DEFAULT 0 COMMENT '是否审核',
  `uid` int(10) NULL DEFAULT 0 COMMENT 'uid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_comment
-- ----------------------------

-- ----------------------------
-- Table structure for wp_common_category
-- ----------------------------
DROP TABLE IF EXISTS `wp_common_category`;
CREATE TABLE `wp_common_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类标识',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类标题',
  `pid` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '上一级分类',
  `app` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属应用',
  `sort` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '排序号',
  `is_show` tinyint(2) NULL DEFAULT 1 COMMENT '是否显示',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_common_category
-- ----------------------------

-- ----------------------------
-- Table structure for wp_common_category_link
-- ----------------------------
DROP TABLE IF EXISTS `wp_common_category_link`;
CREATE TABLE `wp_common_category_link`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_id` int(10) NULL DEFAULT 0 COMMENT '分类ID',
  `app` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用名',
  `data_id` int(10) NULL DEFAULT 0 COMMENT '应用数据ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_common_category_link
-- ----------------------------

-- ----------------------------
-- Table structure for wp_common_category_meta
-- ----------------------------
DROP TABLE IF EXISTS `wp_common_category_meta`;
CREATE TABLE `wp_common_category_meta`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_id` int(10) NOT NULL DEFAULT 0 COMMENT '分类ID',
  `meta_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'key',
  `meta_value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'value',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wp_common_category_meta
-- ----------------------------

-- ----------------------------
-- Table structure for wp_config
-- ----------------------------
DROP TABLE IF EXISTS `wp_config`;
CREATE TABLE `wp_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置类型',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置分组',
  `extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置说明',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置值',
  `sort` smallint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of wp_config
-- ----------------------------
INSERT INTO `wp_config` VALUES (1, 'WEB_SITE_TITLE', 1, '网站标题', 1, '', '网站标题前台显示标题', 1378898976, 1430825115, 1, '小韦云后台框架', 0);
INSERT INTO `wp_config` VALUES (2, 'WEB_SITE_DESCRIPTION', 2, '网站描述', 1, '', '网站搜索引擎描述', 1378898976, 1379235841, 1, '小韦云后台框架是出色的后台管理框架，可快速搭建各类业务系统', 9);
INSERT INTO `wp_config` VALUES (3, 'WEB_SITE_KEYWORD', 2, '网站关键字', 1, '', '网站搜索引擎关键字', 1378898976, 1381390100, 1, '小韦云 后台框架 小韦云科技', 8);
INSERT INTO `wp_config` VALUES (4, 'WEB_SITE_CLOSE', 4, '关闭站点', 1, '0:关闭\r\n1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', 1378898976, 1406859591, 1, '1', 1);
INSERT INTO `wp_config` VALUES (9, 'CONFIG_TYPE_LIST', 3, '配置类型列表', 6, '', '主要用于数据解析和页面表单的生成', 1378898976, 1379235348, 1, '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举', 2);
INSERT INTO `wp_config` VALUES (10, 'WEB_SITE_ICP', 1, '网站备案号', 1, '', '设置在网站底部显示的备案号，如“沪ICP备12007941号-2', 1378900335, 1379235859, 1, '', 9);
INSERT INTO `wp_config` VALUES (20, 'CONFIG_GROUP_LIST', 3, '配置分组', 6, '', '配置分组', 1379228036, 1384418383, 1, '1:基本\r\n3:用户\r\n6:开发\r\n99:高级', 4);
INSERT INTO `wp_config` VALUES (32, 'DEVELOP_MODE', 4, '开启开发者模式', 6, '0:关闭\r\n1:开启', '是否开启开发者模式', 1383105995, 1383291877, 1, '0', 0);
INSERT INTO `wp_config` VALUES (36, 'ADMIN_ALLOW_IP', 2, '后台允许访问IP', 99, '', '多个用逗号分隔，如果不配置表示不限制IP访问', 1387165454, 1387165553, 1, '', 12);
INSERT INTO `wp_config` VALUES (38, 'WEB_SITE_VERIFY', 4, '登录验证码', 3, '0:关闭\r\n1:开启', '登录时是否需要验证码', 1378898976, 1406859544, 1, '0', 2);
INSERT INTO `wp_config` VALUES (42, 'ACCESS', 2, '未登录时可访问的页面', 6, '', '不区分大小写', 1390656601, 1390664079, 1, 'Home/User/*\r\nHome/Index/*\r\nHome/Help/*\r\nhome/weixin/*\r\nadmin/File/*\r\nhome/File/*\r\nhome/Forum/*\r\nHome/Material/detail', 0);
INSERT INTO `wp_config` VALUES (45, 'SYSTEM_UPDATE_REMIND', 4, '系统升级提醒', 6, '0:关闭\r\n1:开启', '开启后官方有新升级信息会及时在后台的网站设置页面头部显示升级提醒', 1393764263, 1393764263, 1, '0', 5);
INSERT INTO `wp_config` VALUES (46, 'SYSTEM_UPDATRE_VERSION', 0, '系统升级最新版本号', 0, '', '记录当前系统的版本号，这是与官方比较是否有升级包的唯一标识，不熟悉者只勿改变其数值', 1393764702, 1394337646, 1, '20160708', 0);
INSERT INTO `wp_config` VALUES (47, 'FOLLOW_YOUKE_UID', 0, '粉丝游客ID', 0, '', '', 1398927704, 1398927704, 1, '-16570', 0);
INSERT INTO `wp_config` VALUES (50, 'COPYRIGHT', 1, '版权信息', 1, '', '', 1401018910, 1401018910, 1, '版本由小韦云科技有限公司所有', 3);
INSERT INTO `wp_config` VALUES (52, 'SYSTEM_LOGO', 1, '网站LOGO的URL', 0, '', '填写LOGO的网址，为空时默认显示bctos的logo', 1403566699, 1403566746, 1, '', 0);
INSERT INTO `wp_config` VALUES (60, 'TONGJI_CODE', 2, '第三方统计JS代码', 99, '', '', 1428634717, 1428634717, 1, '', 0);
INSERT INTO `wp_config` VALUES (61, 'SENSITIVE_WORDS', 1, '敏感词', 1, '', '当出现有敏感词的地方，会用*号代替, (多个敏感词用 , 隔开 )', 1433125977, 1463195869, 1, 'bitch,shit', 11);
INSERT INTO `wp_config` VALUES (62, 'REG_AUDIT', 4, '注册审核', 3, '0:需要审核\r\n1:不需要审核', '', 1439811099, 1439811099, 1, '1', 1);
INSERT INTO `wp_config` VALUES (63, 'PUBLIC_BIND', 4, '公众号第三方平台', 5, '0:关闭\r\n1:开启', '申请审核通过微信开放平台里的公众号第三方平台账号后，就可以开启体验了', 1434542818, 1434542818, 1, '0', 0);
INSERT INTO `wp_config` VALUES (64, 'COMPONENT_APPID', 1, '公众号开放平台的AppID', 5, '', '公众号第三方平台开启后必填的参数', 1434542891, 1434542975, 1, 'wxedd687dfab20466a', 0);
INSERT INTO `wp_config` VALUES (65, 'COMPONENT_APPSECRET', 1, '公众号开放平台的AppSecret', 5, '', '公众号第三方平台开启后必填的参数', 1434542936, 1434542984, 1, 'd159ffea0012654a0cefaf91991ff6ed', 0);
INSERT INTO `wp_config` VALUES (67, 'APPID', 2, '小程序AppID', 0, '', '', 1477122750, 1477122750, 1, '', 0);
INSERT INTO `wp_config` VALUES (68, 'APPSECRET', 1, '小程序AppSecret', 0, '', '', 1477122812, 1477122812, 1, '', 0);
INSERT INTO `wp_config` VALUES (69, 'USER_ALLOW_REGISTER', 4, '是否允许用户注册', 3, '0:关闭注册\r\n1:允许注册', '是否开放用户注册', 0, 0, 1, '1', 0);
INSERT INTO `wp_config` VALUES (72, 'IS_QRCODE_LOGIN', 4, '是否开启扫码登录', 10, '0:否\r\n1:是', '是否开启扫码登录', 0, 0, 1, '0', 0);
INSERT INTO `wp_config` VALUES (73, 'DEFAULT_PUBLICS', 4, '扫码登录绑定的公众号', 10, '1:一机一码\n-1:官方公众号\n', '', 0, 0, 1, '', 3);
INSERT INTO `wp_config` VALUES (74, 'REQUEST_LOG', 4, '接口日志是否开启', 0, '0:否\r\n1:是', '', 0, 0, 1, '1', 0);
INSERT INTO `wp_config` VALUES (75, 'SCAN_LOGIN', 4, '是否开启扫码登录', 10, '0:关闭\r\n1:开启', '', 0, 0, 0, '0', 0);
INSERT INTO `wp_config` VALUES (76, 'ENCODING_AES_KEY', 1, '公众号消息加解密Key', 5, '', '', 0, 0, 1, 'DfEqNBRvzbg8MJdRQCSGyaMp6iLcGOldKFT0r8I6Tnp', 0);
INSERT INTO `wp_config` VALUES (77, 'PUBLIC_TYPE', 4, '账号开放', 0, '0:同时显示公众号和小程序\r\n1:只显示公众号\r\n2:只显示小程序\r\n3:不启用公众号或小程序', '', 0, 0, 1, '2', 100);

-- ----------------------------
-- Table structure for wp_credit_config
-- ----------------------------
DROP TABLE IF EXISTS `wp_credit_config`;
CREATE TABLE `wp_credit_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则名称',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则标识名',
  `mod` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用英文名，核心功能默认为common',
  `mTime` int(10) NULL DEFAULT 0 COMMENT '更新时间',
  `score` int(10) NULL DEFAULT 0 COMMENT '积分值',
  `type` tinyint(1) NULL DEFAULT 0 COMMENT '规则类型 0是公众号积分规则 1是非公众号积分规则 2是可变积分规则',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_credit_config
-- ----------------------------

-- ----------------------------
-- Table structure for wp_credit_data
-- ----------------------------
DROP TABLE IF EXISTS `wp_credit_data`;
CREATE TABLE `wp_credit_data`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `credit_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规则标识名',
  `credit_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '积分描述',
  `score` int(10) NULL DEFAULT 0 COMMENT '积分值',
  `cTime` int(10) NOT NULL DEFAULT 0 COMMENT '记录时间',
  `admin_uid` int(10) NULL DEFAULT 0 COMMENT '操作者UID，0表示系统自动增加',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_credit_data
-- ----------------------------

-- ----------------------------
-- Table structure for wp_credit_grade
-- ----------------------------
DROP TABLE IF EXISTS `wp_credit_grade`;
CREATE TABLE `wp_credit_grade`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '等级名称',
  `icon` int(10) NULL DEFAULT NULL COMMENT '等级图标',
  `mTime` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `score` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '累计积分要求的值',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_credit_grade
-- ----------------------------

-- ----------------------------
-- Table structure for wp_debug_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_debug_log`;
CREATE TABLE `wp_debug_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `data_post` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cTime_format` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cTime` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_debug_log
-- ----------------------------

-- ----------------------------
-- Table structure for wp_file
-- ----------------------------
DROP TABLE IF EXISTS `wp_file`;
CREATE TABLE `wp_file`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `md5` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件保存位置',
  `create_time` int(10) UNSIGNED NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_md5`(`md5`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_file
-- ----------------------------

-- ----------------------------
-- Table structure for wp_import
-- ----------------------------
DROP TABLE IF EXISTS `wp_import`;
CREATE TABLE `wp_import`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `attach` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传文件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_import
-- ----------------------------

-- ----------------------------
-- Table structure for wp_keyword
-- ----------------------------
DROP TABLE IF EXISTS `wp_keyword`;
CREATE TABLE `wp_keyword`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keyword` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键词',
  `addon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键词所属插件',
  `aim_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '插件表里的ID值',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `keyword_length` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '关键词长度',
  `keyword_type` tinyint(2) NULL DEFAULT 0 COMMENT '匹配类型',
  `extra_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文本扩展',
  `extra_int` int(10) NULL DEFAULT 0 COMMENT '数字扩展',
  `request_count` int(10) NULL DEFAULT 0 COMMENT '请求数',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `keyword_token`(`keyword`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_keyword
-- ----------------------------

-- ----------------------------
-- Table structure for wp_manager
-- ----------------------------
DROP TABLE IF EXISTS `wp_manager`;
CREATE TABLE `wp_manager`  (
  `uid` int(10) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `has_public` tinyint(2) NULL DEFAULT 0 COMMENT '是否配置公众号',
  `headface_url` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '管理员头像',
  `GammaAppId` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摇电视的AppId',
  `GammaSecret` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '摇电视的Secret',
  `copy_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权信息',
  `tongji_code` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '统计代码',
  `website_logo` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '网站LOGO',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_manager
-- ----------------------------

-- ----------------------------
-- Table structure for wp_menu
-- ----------------------------
DROP TABLE IF EXISTS `wp_menu`;
CREATE TABLE `wp_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '上级菜单',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名',
  `url_type` tinyint(2) NULL DEFAULT 0 COMMENT '链接类型',
  `addon_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插件名',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外链',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `is_hide` tinyint(2) NULL DEFAULT 0 COMMENT '是否隐藏',
  `sort` int(10) NULL DEFAULT 0 COMMENT '排序号',
  `place` tinyint(1) NULL DEFAULT 0 COMMENT '0：运营端，1：开发端',
  `target` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开方式',
  `rule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '2' COMMENT '权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 899 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_menu
-- ----------------------------
INSERT INTO `wp_menu` VALUES (1, '0', '开发功能', 1, '', 'admin/Apps/index', 'layui-icon layui-icon-util', 0, 99, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (2, '1', '应用管理', 1, '', 'admin/Apps/index', NULL, 0, 1, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (3, '1', '数据模型', 1, '', 'admin/Model/index', NULL, 0, 2, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (4, '1', '菜单管理', 1, '', 'admin/Menu/lists', NULL, 0, 3, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (7, '1', '网站设置', 1, '', 'admin/Config/group', NULL, 0, 6, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (8, '1', '配置管理', 1, '', 'admin/Config/index', NULL, 0, 7, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (12, '1', '清除缓存', 1, '', 'admin/Update/delcache', NULL, 0, 11, 0, '_self', NULL);
INSERT INTO `wp_menu` VALUES (852, '1', '权限配置', 1, '', 'admin/rules/index', '', 1, 8, 0, '_self', '2');
INSERT INTO `wp_menu` VALUES (888, '1', '在线升级', 1, '', 'admin/Update/index', '', 0, 8, 0, '_self', '2');

-- ----------------------------
-- Table structure for wp_message
-- ----------------------------
DROP TABLE IF EXISTS `wp_message`;
CREATE TABLE `wp_message`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bind_keyword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联关键词',
  `preview_openids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '预览人OPENID',
  `group_id` int(10) NULL DEFAULT 0 COMMENT '群发对象',
  `type` tinyint(2) NULL DEFAULT 0 COMMENT '素材来源',
  `media_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信素材ID',
  `send_type` tinyint(1) NULL DEFAULT 0 COMMENT '发送方式',
  `send_openids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要发送的OpenID',
  `msg_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'msg_id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本消息内容',
  `msgtype` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型',
  `appmsg_id` int(10) NULL DEFAULT 0 COMMENT '图文id',
  `voice_id` int(10) NULL DEFAULT 0 COMMENT '语音id',
  `video_id` int(10) NULL DEFAULT 0 COMMENT '视频id',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '群发时间',
  `pbid` int(10) NOT NULL DEFAULT 0 COMMENT 'pbid',
  `image_id` int(10) NULL DEFAULT 0 COMMENT '图片ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_message
-- ----------------------------

-- ----------------------------
-- Table structure for wp_model
-- ----------------------------
DROP TABLE IF EXISTS `wp_model`;
CREATE TABLE `wp_model`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模型标识',
  `title` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模型名称',
  `extend` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '继承的模型',
  `relation` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '继承与被继承模型的关联字段',
  `need_pk` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '新建表时是否需要主键字段',
  `field_sort` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '表单字段排序',
  `field_group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1:基础' COMMENT '字段分组',
  `attribute_list` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '属性列表（表的字段）',
  `template_list` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '编辑模板',
  `list_grid` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '列表定义',
  `list_row` smallint(2) UNSIGNED NULL DEFAULT 10 COMMENT '列表数据长度',
  `search_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '默认搜索字段',
  `search_list` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '高级搜索的字段',
  `create_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(3) UNSIGNED NULL DEFAULT 0 COMMENT '状态',
  `engine_type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'MyISAM' COMMENT '数据库引擎',
  `addon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属插件',
  `file_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2381 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统模型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_model
-- ----------------------------
INSERT INTO `wp_model` VALUES (1, 'user', '用户信息表', 0, '', 0, '[\"come_from\",\"nickname\",\"password\",\"truename\",\"mobile\",\"email\",\"sex\",\"headimgurl\",\"city\",\"province\",\"country\",\"language\",\"score\",\"unionid\",\"login_count\",\"reg_ip\",\"reg_time\",\"last_login_ip\",\"last_login_time\",\"status\",\"is_init\",\"is_audit\"]', '1:基础', '', '', '', '', 'headimgurl|url_img_html:头像\r\nlogin_name:登录账号\r\nlogin_password:登录密码\r\nnickname|deal_emoji:用户昵称\r\nsex|get_name_by_status:性别\r\ngroup:分组\r\nscore:金币值\r\nids:操作:set_login?uid=[uid]|设置登录账号,detail?uid=[uid]|详细资料,[EDIT]|编辑', 20, '', '', 1436929111, 1441187405, 1, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (3, 'menu', '公众号管理员菜单', 0, '', 1, '[\"menu_type\",\"pid\",\"title\",\"url_type\",\"addon_name\",\"url\",\"target\",\"is_hide\",\"sort\"]', '1:基础', '', '', '', '', 'title:菜单名\r\nmenu_type|get_name_by_status:菜单类型\r\naddon_name:插件名\r\nurl:外链\r\ntarget|get_name_by_status:打开方式\r\nis_hide|get_name_by_status:隐藏\r\nsort:排序号\r\nids:操作:[EDIT]|编辑,[DELETE]|删除', 65535, '', '', 1435215960, 1437623073, 1, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (20, 'auth_group', '用户组', 0, '', 1, '[\"title\",\"description\"]', '1:基础', '', '', '', '', 'title:分组名称\r\ndescription:描述\r\nqr_code:二维码\r\nids:操作:export?id=[id]|导出用户,[EDIT]|编辑,[DELETE]|删除', 20, 'title', '', 1437633503, 1447660681, 1, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (1225, 'public_config', '公共配置信息', 0, '', 1, NULL, '1:基础', NULL, '', '', '', NULL, 10, '', '', 0, 0, 0, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (2347, 'app_shop', '扩展商城', 0, '', 1, NULL, '1:基础', NULL, '', '', '', NULL, 20, '', '', 0, 0, 0, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (2350, 'app_shop_web', '网站信息', 0, '', 1, NULL, '1:基础', NULL, '', '', '', NULL, 20, '', '', 0, 0, 0, 'MyISAM', 'core', NULL);
INSERT INTO `wp_model` VALUES (2351, 'app_shop_user', '用户下载记录', 0, '', 1, NULL, '1:基础', NULL, '', '', '', NULL, 20, '', '', 0, 0, 0, 'MyISAM', 'admin', NULL);
INSERT INTO `wp_model` VALUES (2366, 'block', '块元素', 0, '', 1, NULL, '1:基础', NULL, '', '', '', NULL, 20, '', '', 0, 0, 0, 'MyISAM', 'core', NULL);

-- ----------------------------
-- Table structure for wp_notice
-- ----------------------------
DROP TABLE IF EXISTS `wp_notice`;
CREATE TABLE `wp_notice`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_notice
-- ----------------------------

-- ----------------------------
-- Table structure for wp_phinxlog
-- ----------------------------
DROP TABLE IF EXISTS `wp_phinxlog`;
CREATE TABLE `wp_phinxlog`  (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp(0) NULL DEFAULT NULL,
  `end_time` timestamp(0) NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_phinxlog
-- ----------------------------

-- ----------------------------
-- Table structure for wp_picture
-- ----------------------------
DROP TABLE IF EXISTS `wp_picture`;
CREATE TABLE `wp_picture`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '状态',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `system` tinyint(10) NULL DEFAULT 0,
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  `social_id` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`id`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12049 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_picture
-- ----------------------------

-- ----------------------------
-- Table structure for wp_plugin
-- ----------------------------
DROP TABLE IF EXISTS `wp_plugin`;
CREATE TABLE `wp_plugin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置',
  `author` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '作者',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '版本号',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装时间',
  `has_adminlist` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否有后台列表',
  `cate_id` int(11) NULL DEFAULT NULL,
  `is_show` tinyint(2) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE,
  INDEX `sti`(`status`, `is_show`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 130 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统插件表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for wp_public_auth
-- ----------------------------
DROP TABLE IF EXISTS `wp_public_auth`;
CREATE TABLE `wp_public_auth`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type_0` tinyint(1) NULL DEFAULT 0 COMMENT '普通订阅号的开关',
  `type_1` tinyint(1) NULL DEFAULT 0 COMMENT '微信认证订阅号的开关',
  `type_2` tinyint(1) NULL DEFAULT 0 COMMENT '普通服务号的开关',
  `type_3` tinyint(1) NULL DEFAULT 0 COMMENT '微信认证服务号的开关',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_public_auth
-- ----------------------------

-- ----------------------------
-- Table structure for wp_public_check
-- ----------------------------
DROP TABLE IF EXISTS `wp_public_check`;
CREATE TABLE `wp_public_check`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `na` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 396 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_public_check
-- ----------------------------

-- ----------------------------
-- Table structure for wp_public_config
-- ----------------------------
DROP TABLE IF EXISTS `wp_public_config`;
CREATE TABLE `wp_public_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pbid` int(11) NULL DEFAULT 0,
  `pkey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置规则名',
  `pvalue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置值',
  `mtime` int(10) NULL DEFAULT 0 COMMENT '设置时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 133 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_public_config
-- ----------------------------
INSERT INTO `wp_public_config` VALUES (131, 0, 'remote_tag', '0.0.2', 1607499457);
INSERT INTO `wp_public_config` VALUES (132, 0, 'local_tag', '0.0.2', 1607654684);

-- ----------------------------
-- Table structure for wp_public_follow
-- ----------------------------
DROP TABLE IF EXISTS `wp_public_follow`;
CREATE TABLE `wp_public_follow`  (
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `uid` int(11) NULL DEFAULT NULL,
  `has_subscribe` tinyint(1) NULL DEFAULT 0,
  `syc_status` tinyint(1) NULL DEFAULT 2 COMMENT '0 开始同步中 1 更新用户信息中 2 完成同步',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `unionid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '微信第三方ID',
  `pbid` int(10) NOT NULL DEFAULT 0 COMMENT 'pbid',
  UNIQUE INDEX `openid`(`openid`, `pbid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_public_follow
-- ----------------------------

-- ----------------------------
-- Table structure for wp_publics
-- ----------------------------
DROP TABLE IF EXISTS `wp_publics`;
CREATE TABLE `wp_publics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) NULL DEFAULT 0 COMMENT '用户ID',
  `public_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公众号名称',
  `public_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公众号原始id',
  `interface_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口地址',
  `headface_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '公众号头像',
  `area` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地区',
  `addon_status` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '插件状态',
  `is_use` tinyint(2) NULL DEFAULT 0 COMMENT '是否为当前公众号',
  `type` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公众号类型',
  `appid` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AppID',
  `secret` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AppSecret',
  `encodingaeskey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EncodingAESKey',
  `tips_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提示关注公众号的文章地址',
  `domain` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义域名',
  `is_bind` tinyint(2) NULL DEFAULT 0 COMMENT '是否为微信开放平台绑定账号',
  `mch_id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商户号',
  `partner_key` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付密钥',
  `cert_pem` int(11) NULL DEFAULT 0 COMMENT '证书cert',
  `key_pem` int(11) NULL DEFAULT 0 COMMENT '证书key',
  `authorizer_refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'authorizer_refresh_token',
  `check_file` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '微信验证文件',
  `app_type` tinyint(2) NULL DEFAULT 0 COMMENT '公众号类型',
  `wpid` int(11) NULL DEFAULT 0 COMMENT 'wpid',
  `order_payok_messageid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易完成通知的模板ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_publics
-- ----------------------------
INSERT INTO `wp_publics` VALUES (1, 1, '小韦云', 'gh_646280fabe21', NULL, NULL, NULL, NULL, 0, '1', 'wxbe8c175aa84aef8a', 'bca9cc806e370b7f5754c52a3ada5979', '', NULL, NULL, 0, '1488433945', 'bca9cc806e370b7f5754c52a3ada5989', NULL, NULL, NULL, NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for wp_qr_admin
-- ----------------------------
DROP TABLE IF EXISTS `wp_qr_admin`;
CREATE TABLE `wp_qr_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `group_id` int(10) NULL DEFAULT 0 COMMENT '用户组',
  `tag_ids` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户标签',
  `qr_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二维码',
  `material` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扫码后的回复内容',
  `mult_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多图上传',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_qr_admin
-- ----------------------------

-- ----------------------------
-- Table structure for wp_qr_code
-- ----------------------------
DROP TABLE IF EXISTS `wp_qr_code`;
CREATE TABLE `wp_qr_code`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `qr_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '二维码',
  `addon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '二维码所属插件',
  `aim_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '插件表里的ID值',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `action_name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二维码类型',
  `extra_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本扩展',
  `extra_int` int(10) NULL DEFAULT 0 COMMENT '数字扩展',
  `request_count` int(10) NULL DEFAULT 0 COMMENT '请求数',
  `scene_id` int(10) NULL DEFAULT 0 COMMENT '场景ID',
  `expire_seconds` int(11) NULL DEFAULT 2592000 COMMENT '有效期',
  `pbid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_qr_code
-- ----------------------------

-- ----------------------------
-- Table structure for wp_request_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_request_log`;
CREATE TABLE `wp_request_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `md5` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '序列化后的参数',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `res` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `error_code` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `server_ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务器IP地址',
  `cTime` int(10) NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_request_log
-- ----------------------------

-- ----------------------------
-- Table structure for wp_sn_code
-- ----------------------------
DROP TABLE IF EXISTS `wp_sn_code`;
CREATE TABLE `wp_sn_code`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SN码',
  `uid` int(10) NULL DEFAULT 0 COMMENT '粉丝UID',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `is_use` tinyint(2) NULL DEFAULT 0 COMMENT '是否已使用',
  `use_time` int(10) NULL DEFAULT 0 COMMENT '使用时间',
  `target_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '来源ID',
  `prize_id` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '奖项ID',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '是否可用',
  `prize_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖项',
  `can_use` tinyint(2) NULL DEFAULT 1 COMMENT '是否可用',
  `server_addr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务器IP',
  `admin_uid` int(10) NULL DEFAULT 0 COMMENT '核销管理员ID',
  `wpid` int(10) NOT NULL DEFAULT 0 COMMENT 'wpid',
  `openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'openid',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`uid`, `target_id`) USING BTREE,
  INDEX `addon`(`target_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_sn_code
-- ----------------------------

-- ----------------------------
-- Table structure for wp_system_notice
-- ----------------------------
DROP TABLE IF EXISTS `wp_system_notice`;
CREATE TABLE `wp_system_notice`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公告内容',
  `create_time` int(10) NULL DEFAULT 0 COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_system_notice
-- ----------------------------

-- ----------------------------
-- Table structure for wp_update_version
-- ----------------------------
DROP TABLE IF EXISTS `wp_update_version`;
CREATE TABLE `wp_update_version`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `version` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '版本号',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '升级包名',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `create_date` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `download_count` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '下载统计',
  `package` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '升级包地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_update_version
-- ----------------------------

-- ----------------------------
-- Table structure for wp_user
-- ----------------------------
DROP TABLE IF EXISTS `wp_user`;
CREATE TABLE `wp_user`  (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `truename` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `mobile` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱地址',
  `sex` tinyint(2) NULL DEFAULT 0 COMMENT '性别',
  `headimgurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '城市',
  `province` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `country` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `language` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '语言',
  `score` int(10) NULL DEFAULT 0 COMMENT '积分值',
  `unionid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信第三方ID',
  `experience` int(10) NULL DEFAULT 0 COMMENT '经验值',
  `login_count` int(10) NULL DEFAULT 0 COMMENT '登录次数',
  `reg_ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '注册IP',
  `reg_time` int(10) NULL DEFAULT 0 COMMENT '注册时间',
  `last_login_ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最近登录IP',
  `last_login_time` int(10) NULL DEFAULT 0 COMMENT '最近登录时间',
  `status` tinyint(2) NULL DEFAULT 1 COMMENT '状态',
  `is_init` tinyint(2) NULL DEFAULT 0 COMMENT '初始化状态',
  `is_audit` tinyint(2) NULL DEFAULT 0 COMMENT '审核状态',
  `subscribe_time` int(10) NULL DEFAULT 0 COMMENT '用户关注公众号时间',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信用户备注',
  `groupid` int(10) NULL DEFAULT 0 COMMENT '微信端的分组ID',
  `come_from` tinyint(1) NULL DEFAULT 0 COMMENT '来源',
  `login_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'login_name',
  `login_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `manager_id` int(10) NULL DEFAULT 0 COMMENT '公众号管理员ID',
  `level` tinyint(2) NULL DEFAULT 0 COMMENT '-1:机器人0:粉丝1:超级管理员2:A级管理员\r\n3:B级管理员\r\n4:C级管理员',
  `membership` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '会员等级',
  `bind_openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '绑定的openid',
  `audit_time` int(10) NULL DEFAULT 0 COMMENT '审核通过时间',
  `grade` int(10) NULL DEFAULT 0 COMMENT '当前用户的等级',
  `wpid` int(11) NULL DEFAULT 0,
  `tagid_list` varchar(555) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户被打上的标签ID列表',
  `in_blacklist` tinyint(2) NULL DEFAULT 0 COMMENT '是否拉黑',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '个性签名',
  `reward_money` float NULL DEFAULT 0 COMMENT '钱包',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'DZ密码加密KEY',
  `social_id` int(10) NULL DEFAULT 1 COMMENT '社区ID',
  `social_rule` int(10) NULL DEFAULT 0 COMMENT '社区角色',
  `weiba_id` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '吧主权限，版块id',
  `userid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '加密用户标识',
  `industry` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行业',
  `hobby` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '爱好',
  `birthday` int(10) NULL DEFAULT 0 COMMENT 'birthday',
  `area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'area',
  `audit_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核不通过的原因',
  `price` int(10) NULL DEFAULT 20 COMMENT '单价',
  `copyright` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版权信息',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53763 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_user
-- ----------------------------
INSERT INTO `wp_user` VALUES (1, 'admin', '95f78b3dde0f5d3c4dca7f0e2ad7acf2', '超级管理员', '18123611365', 'admin@bctos.cn', 1, 'https://wx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEJFBe2dVib0WHhQy4oSoR6jJR4ytwtAS5iaTibL06SJwiaghzOfpsGkc67IIYk6xKvZ73LCsrukDHsVbA/132', 'Shenzhen', 'Guangdong', 'China', 'zh_CN', 87970, NULL, 0, 2108, '0', 1474905117, '0', 1608542840, 1, 1, 1, 1532575436, '', 0, 0, 'admin', '123456', 0, 2, '0', 'o4jRyt2BdS79xCkWER1bDsBzRYCo', NULL, 0, 86, NULL, 0, 'Room 2103 Zhianshanwu Building', '666', 0, NULL, 1, 0, NULL, NULL, '3428,3429', '3426,3427', 1262361600, '浙江省 杭州市 上城区', NULL, 20, NULL);

-- ----------------------------
-- Table structure for wp_visit_log
-- ----------------------------
DROP TABLE IF EXISTS `wp_visit_log`;
CREATE TABLE `wp_visit_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `wpid` int(10) NULL DEFAULT 0 COMMENT 'publicid',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'module_name',
  `controller_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'controller_name',
  `action_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'action_name',
  `uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT 'uid',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip',
  `brower` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'brower',
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'param',
  `referer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'referer',
  `cTime` int(10) NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70709 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wp_visit_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
